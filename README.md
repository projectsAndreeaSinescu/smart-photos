# Smart photos
Este o aplicatie web, dezvoltata cu ASP.NET si stilizata cu Bootstrap, asemanatoare cu Flickr.
In cadrul acesteia, adminul poate adauga categorii, gestiona utilizatorii si comentariile din cadrul aplicatiei, iar orice utilizator poate adauga fotografii in categorii, 
poate adauga comentarii, poate aprecia fotografiile si poate adauga albume private cu
fotografii. Utilizatorul dispune si de un sistem de recomandare in functie de categoriile din care fac parte fotografiile apreciate.

## Cautare si afisare fotografii
![](./docs/cautare_fotografii.PNG)

## Gestionarea utilizatorilor de catre admin
![](./docs/gestionare_useri.PNG)