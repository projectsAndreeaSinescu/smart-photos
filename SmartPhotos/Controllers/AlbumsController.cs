﻿using Microsoft.AspNet.Identity;
using SmartPhotos.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPhotos.Controllers
{
    public class AlbumsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Albums
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Index()
        {
            var user = User.Identity.GetUserId();
            var albums = from album in db.Albums
                         where album.UserId == user
                         orderby album.Title
                         select album;
            ViewBag.Albums = albums;
            ViewBag.currentUser = User.Identity.GetUserId();
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            var alb = db.Albums.Include("User");
            ViewBag.alb = alb;
            if (User.IsInRole("Administrator"))
            {
                ViewBag.esteAdmin = true;
            }
            else
            {
                ViewBag.esteAdmin = false;
            }
            return View();
        }
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Show(int id)
        {
            Album album = db.Albums.Find(id);
            var images = from image in db.Photos
                         where image.IdAlbum == id
                         select image;
            images = images.OrderByDescending(i => i.CreatedDate);
            ViewBag.Images = images;
            ViewBag.currentUser = User.Identity.GetUserId();
            var alb = db.Albums.Include("User");
            ViewBag.alb = alb;
            if (User.IsInRole("Administrator"))
            {
                ViewBag.esteAdmin = true;
            }
            else
            {
                ViewBag.esteAdmin = false;
            }
            return View(album);
        }

        [Authorize(Roles = "User,Administrator")]
        public ActionResult AddPhoto(int id)
        {
            PhotoViewModel photo = new PhotoViewModel();
            //preluam lista de categorii din metoda GetAllCategories()
            photo.Categories = GetAllCategories();
            //preluam id-ul userul curent
            photo.UserId = User.Identity.GetUserId();
            photo.IdAlbum = id;
            return View(photo);
        }
        [NonAction]
        public IEnumerable<SelectListItem> GetAllCategories()
        {
            // generam o lista goala
            var selectList = new List<SelectListItem>();
            // Extragem toate categoriile din baza de date
            var categories = from cat in db.Categories select cat;
            // iteram prin categorii
            foreach (var category in categories)
            {
                // Adaugam in lista elementele necesare pentru dropdown
                selectList.Add(new SelectListItem
                {
                    Value = category.Id.ToString(),
                    Text = category.Nume.ToString()
                });
            }
            // returnam lista de categorii
            return selectList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult AddPhoto(int id, PhotoViewModel model)
        {
            model.Categories = GetAllCategories();
            model.IdAlbum = id;
            try
            {
                var validImageTypes = new string[]
                {
                    "image/gif",
                    "image/jpeg",
                    "image/pjpeg",
                    "image/png"
                };

                if (model.ImageUpload == null || model.ImageUpload.ContentLength == 0)
                {
                    ModelState.AddModelError("ImageUpload", "Image Upload field is required");
                }
                else if (!validImageTypes.Contains(model.ImageUpload.ContentType))
                {
                    ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
                }

                if (ModelState.IsValid)
                {
                    var image = new Photo
                    {
                        Title = model.Title,
                        AltText = model.AltText,
                        Caption = model.Caption,
                        IdCategory = model.IdCategory,
                        UserId = model.UserId,
                        IdAlbum = id
                    };

                    if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                    {
                        var uploadDir = "~/uploads";
                        var numeImg = model.ImageUpload.FileName;
                        var imagePath = Path.Combine(Server.MapPath(uploadDir), numeImg);
                        var imageUrl = Path.Combine(uploadDir, numeImg);
                        model.ImageUpload.SaveAs(imagePath);
                        image.ImageUrl = imageUrl;
                    }

                    db.Photos.Add(image);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                return View(model);
            }

            return View(model);
        }

        [Authorize(Roles = "User,Administrator")]
        public ActionResult New()
        {
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult New(Album model)
        {
            try
            {
                var album = new Album
                {
                    Title = model.Title,
                    UserId = User.Identity.GetUserId()
                };
                db.Albums.Add(album);
                db.SaveChanges();
                TempData["message"] = "Albumul a fost creat!";
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Edit(int id)
        {
            Album album = db.Albums.Find(id);
            ViewBag.Album = album;
            ViewBag.currentUser = User.Identity.GetUserId();
            return View(album);
        }

        [HttpPut]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Edit(int id, Album requestAlbum)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Album album = db.Albums.Find(id);
                    if (TryUpdateModel(album))
                    {
                        album.Title = requestAlbum.Title;
                        db.SaveChanges();
                        TempData["Message"] = "Albumul a fost modificat!";
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(requestAlbum);
                }
            }
            catch (Exception e)
            {
                return View(requestAlbum);
            }
        }
        [HttpDelete]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Delete(int id)
        {
            Album album = db.Albums.Find(id);
            var photos = from image in db.Photos
                         where image.IdAlbum == id
                         select image;
            db.Photos.RemoveRange(photos);
            db.Albums.Remove(album);
            db.SaveChanges();
            TempData["message"] = "Albumul a fost sters!";
            return RedirectToAction("Index");
        }
    }
}