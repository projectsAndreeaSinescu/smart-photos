﻿using SmartPhotos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPhotos.Controllers
{
    public class CategoriesController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();

        // GET: Categories
        public ActionResult Index()
        {
            var categories = from category in db.Categories
                             orderby category.Nume
                             select category;

            ViewBag.Categories = categories;

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            if (User.IsInRole("Administrator"))
            {
                ViewBag.esteAdmin = true;
            }
            else
            {
                ViewBag.esteAdmin = false;
            }
            return View();
        }

        public ActionResult Show(int id)
        {
            Category category = db.Categories.Find(id);
            //ViewBag.Category = category;
            //pozele din categorie
            var images = from image in db.Photos
                         where image.IdCategory == id
                         select image;
            images = images.OrderByDescending(i => i.CreatedDate);
            ViewBag.Images = images;
            //pentru afisarea butonului de stergere
            if (User.IsInRole("Administrator"))
            {
                ViewBag.esteAdmin = true;
            }
            else
            {
                ViewBag.esteAdmin = false;
            }
            return View(category);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult New(Category category)
        {
            try
            {
                db.Categories.Add(category);
                db.SaveChanges();
                TempData["message"] = "Categoria a fost adaugata!";
                var users = from user in db.Users
                            select user;
                var categories = from cat in db.Categories
                                 where cat.Nume == category.Nume
                                 select cat;
                var c = categories.First();

                foreach (var u in users)
                {
                    Statistic statistic = new Statistic();
                    statistic.CategoryId = c.Id;
                    statistic.ViewCounter = 0;
                    statistic.UserId = u.Id;

                    db.Statistics.Add(statistic);

                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            Category category = db.Categories.Find(id);
            ViewBag.Category = category;
            return View(category);
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id, Category requestCategory)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Category category = db.Categories.Find(id);
                    if (TryUpdateModel(category))
                    {
                        category.Nume = requestCategory.Nume;
                        db.SaveChanges();
                        TempData["message"] = "Categoria a fost modificata!";
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(requestCategory);
                }

            }
            catch (Exception e)
            {
                return View(requestCategory);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            Category category = db.Categories.Find(id);
            var photos = from image in db.Photos
                         where image.IdCategory == id
                         select image;
            db.Photos.RemoveRange(photos);
            db.Categories.Remove(category);
            db.SaveChanges();
            TempData["message"] = "Categoria a fost stearsa!";
            return RedirectToAction("Index");
        }

    }
}