﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartPhotos.Models
{
    public class Album
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string UserId { get; set; }

        public virtual ICollection<Photo> Photos { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}