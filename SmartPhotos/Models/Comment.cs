﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartPhotos.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public int IdPhoto { get; set; }

        public virtual Photo Photo { get; set; }

        [Required]
        public string Status { get; set; }

        public string UserId { get; set; }

        //public DateTime createdDate { get; set; }
        public DateTime createdDate { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}